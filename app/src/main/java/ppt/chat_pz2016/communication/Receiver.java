package ppt.chat_pz2016.communication;

import android.bluetooth.le.ScanResult;
import android.util.Log;

import org.json.simple.parser.ParseException;

import ppt.chat_pz2016.Main;

/**
 * Created by dot on 28.10.16.
 */

/**
 * Class responsible for managing receiving messages from other users via BLE
 */
public class Receiver implements IReceiver {
    /**
     * Currently running listener
     */
    private Daemon daemon;

    /**
     * Start listening to broadcasts
     */
    @Override
    public void listen() {
        spawnDaemon();
        getDaemon().start();
    }

    /**
     * Handle incoming broadcast
     * @param callbackType
     * @param result
     */
    @Override
    public void onReceive(int callbackType, ScanResult result) {
        try {
            Main.dataRouter.receive(result.getScanRecord().getDeviceName(), result.getScanRecord().getBytes());
        } catch (ParseException e) {
            Log.e("Receiver", "Failed to parse scan record!");
            e.printStackTrace();
        }
    }

    /**
     * Spawn new listening process. Looking at the android beacon libary specs
     * it seems that we not need more than one of these.
     */
    private void spawnDaemon() {
        setDaemon(new Daemon(100000, 100));
    }

    /**
     * Daemon getter
     * @return daemon instance
     */
    public Daemon getDaemon() {
        return daemon;
    }

    /**
     * Daemon setter
     * @param daemon - new daemon
     */
    private void setDaemon(Daemon daemon) {
        if(this.daemon != null) {
            this.daemon.shutDown();
        }
        this.daemon = daemon;
    }
}
