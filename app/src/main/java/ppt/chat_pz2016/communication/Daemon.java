package ppt.chat_pz2016.communication;

/**
 * Created by dot on 28.10.16.
 */

import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import ppt.chat_pz2016.Main;
import ppt.chat_pz2016.R;

/**
 * Daemon is thread responsible for listening to nearby android devices
 * that are advertising content for our app
 */
public class Daemon extends Thread {
    /**
     * List of scan filters that incoming advertisement must pass before being parsed
     */
    private List<ScanFilter> scanFilters;
    /**
     * Settings of BLE scanner
     */
    private ScanSettings scanSettings;
    /**
     * Scanner callback class
     */
    private ScanCallback scanCallback;
    /**
     * Atomic value determining whether Daemon is running
     */
    private final AtomicBoolean running;
    /**
     * For how long should scanner scan
     */
    private long waitTillStop;
    /**
     * Delay between scans
     */
    private long waitTillRestart;

    /**
     * waitTillStop getter
     * @return waitTillStop
     */
    public long getWaitTillStop() {
        return waitTillStop;
    }

    /**
     * waitTillStop setter
     * @param waitTillStop - new value for how long should scan last
     */
    public void setWaitTillStop(long waitTillStop) {
        this.waitTillStop = waitTillStop;
    }

    /**
     * waitTillRestart getter
     * @return waitTillRestart
     */
    public long getWaitTillRestart() {
        return waitTillRestart;
    }

    /**
     * waitTillRestart setter
     * @param waitTillRestart - new value for how long should waiting between scans be
     */
    public void setWaitTillRestart(long waitTillRestart) {
        this.waitTillRestart = waitTillRestart;
    }

    /**
     * Whether is daemon running or not
     * @return True if thread started, false otherwise
     */
    public boolean isRunning() {
        return running.get();
    }

    /**
     * Constructor
     * @param waitTillStop - how long should daemon wait till stopping scan
     * @param waitTillRestart - how long should daemon wait till reissuing scan
     */
    public Daemon(long waitTillStop, long waitTillRestart) {
        this.scanFilters = new LinkedList<>();
//        scanFilters.add(new ScanFilter.Builder().setServiceUuid(Main.UUID).build());
        scanFilters.add(new ScanFilter.Builder().build());
        this.scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
        this.running = new AtomicBoolean(false);
        this.scanCallback = new ChatScanCallback();
        this.waitTillRestart = waitTillRestart;
        this.waitTillStop = waitTillStop;
    }

    /**
     * Stop daemon. It may take up to getWaitTillStop()+getWaitTillRestart() milliseconds
     */
    public void shutDown() {
        this.running.set(false);
    }

    /**
     * Run daemon
     */
    @Override
    public void run() {
        try {
            Log.d("Daemon", "Starting daemon");
            BluetoothLeScanner scanner = Main.adapter.getBluetoothLeScanner();

            do {
                if(running.get()) {
                    Log.d("Daemon", "Sleeping");
                    if(waitTillRestart > 0) {
                        Thread.sleep(waitTillRestart);
                    }
                } else {
                    running.set(true);
                }
                Log.d("Daemon", "Starting scan!");
                scanner.startScan(scanFilters, scanSettings, scanCallback);
                if(waitTillStop > 0) {
                    Thread.sleep(waitTillStop);
                }
                Log.d("Daemon", "Stoping scan!");
                scanner.stopScan(scanCallback);
            } while (running.get());
        } catch (InterruptedException e) {
            Log.e("Daemon", "Sleep was interrupted!");
            e.printStackTrace();
        }
    }
}
