package ppt.chat_pz2016.communication;

import ppt.chat_pz2016.Main;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.ParcelUuid;
import android.util.Log;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * Created by kovalson on 2016-11-08.
 */

public class Sender implements ISender
{
    private BluetoothLeAdvertiser advertiser;
    private AdvertiseSettings settings;
    private ParcelUuid pUuid;
    private AdvertiseData aData;
    private boolean checked = false;

    /**
     * Constructor
     * Runs canAdvertise()
     * @throws Exception
     */
    public Sender()
    {
        /*
        if( !this.canAdvertise() )
        {
            throw new Exception( "Your device does not support Bluetooth Low Energy!" );
        }
        */
        System.out.println( "Sender created." );
    }

    /**
     * Method checking if the device is able to broadcast over BLE
     * @return boolean true if can, false else
     */
    @Override
    public boolean canAdvertise()
    {
        if( !Main.adapter.isMultipleAdvertisementSupported() )
        {
            return false;
        }
        return true;
    }

    /**
     * Method broadcasting data over BLE
     * @param data byte array of raw information
     * @throws Exception
     */
    @Override
    public void send( byte[] data ) throws Exception
    {
        // if not checked yet, check if the Sender is available to broadcast over BLE
        if( !this.checked )
        {
            if( !this.canAdvertise() )
            {
                System.out.println( "Your device does not support Bluetooth Low Energy!" );
                throw new Exception( "Your device does not support Bluetooth Low Energy!" );
            }
            this.checked = true;
        }

        // instance of the bluetooth advertiser
        advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();

        // settings for advertiser
        settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode( AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY )   // set the latency of advertising to LOW
                .setTxPowerLevel( AdvertiseSettings.ADVERTISE_TX_POWER_HIGH )       // set the sending power to HIGH
                .setConnectable( false )                                            // set unconnectable
                .build();

        // get the receiver's parcel uuid
        // pUuid = new ParcelUuid( UUID.fromString( String.valueOf( R.string.UUID ) ) );
        pUuid = new ParcelUuid( UUID.fromString( String.valueOf( Main.UUID ) ) );

        // make data packet out of the data and the receiver's parce uuid
        aData = new AdvertiseData.Builder()
                .setIncludeDeviceName( true )
                .addServiceUuid( pUuid )
                // .addServiceData( pUuid, "Data".getBytes( Charset.forName( "UTF-8" ) ) )
                .addServiceData( pUuid, data )
                .build();

        // callback functions
        AdvertiseCallback callback = new AdvertiseCallback()
        {
            @Override
            public void onStartSuccess( AdvertiseSettings settingsInEffect )
            {
                super.onStartSuccess( settingsInEffect );
            }

            @Override
            public void onStartFailure( int errorCode )
            {
                Log.e( "BLE", "Advertising onStartFailure: " + errorCode );
                super.onStartFailure( errorCode );
            }
        };

        // finally advertise the data
        advertiser.startAdvertising( settings, aData, callback );
    }
}
