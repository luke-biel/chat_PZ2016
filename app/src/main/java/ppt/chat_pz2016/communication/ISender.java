package ppt.chat_pz2016.communication;

/**
 * Created by dot on 14.10.16.
 * Modified by kovalson
 */

public interface ISender
{
    /**
     * Method checking if device can broadcast data over BLE
     * @return boolean true if can, false else
     */
    boolean canAdvertise();

    /**
     * Method used to broadcast data over BLE
     * @param data byte array of raw information
     */
    void send( byte[] data ) throws Exception;
}
