package ppt.chat_pz2016.communication;

import android.bluetooth.le.ScanResult;

/**
 * Created by dot on 28.10.16.
 */

public interface IReceiver {
    /**
     * Start listening to incoming messages
     */
    void listen();

    /**
     * Function that handles data received from beacons
     */
    void onReceive(int callbackType, ScanResult result);
}
