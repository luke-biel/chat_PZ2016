package ppt.chat_pz2016.dataRouter;

import org.json.simple.parser.ParseException;

/**
 * Created by dot on 14.10.16.
 */

public interface IDataRouter {
     /*
      * Method used to route user message to sender module
      */
     void broadcast(String message);

     /*
      * Method used by receiver component to convert raw bytes into string and then pass it to user interface
      */
     void receive(String deviceName, byte[] data) throws ParseException;
}
