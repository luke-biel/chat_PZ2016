package ppt.chat_pz2016.dataRouter;

import android.content.Context;
import android.util.Log;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Calendar;

import ppt.chat_pz2016.Main;
import ppt.chat_pz2016.userInterface.ChatMessage;

/**
 * Created by dot on 09.11.16.
 */

public class DataRouter implements IDataRouter {

    public  static Context context;

    public  static Context getContext() {
        return DataRouter.context;
    }

    public static void setContext(Context newContext) {
        DataRouter.context = newContext;
    }

    /*public void onCreate(){
        DataRouter.context = getApplicationtContext();
    }*/

    public byte[] JSONtoBytes (JSONObject messageJSON){
        return messageJSON.toString().getBytes();
    }

    public JSONObject translateToJSON(byte[] data) throws ParseException{
        String JsonString = new String(data);
        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(JsonString);
    }

    public JSONObject translateToJSON(String message){
        JSONObject messageJSON = new JSONObject() ;
        messageJSON.put("message", message);
        return messageJSON;
    }

    public String getMessageFromJSON(JSONObject messageJSON){
        return (String) messageJSON.get("message");
    }


    /*if( ! CheckMessage(message) )
    {
        throw new Exception( message + "is not valid message" );
    }*/

    @Override
    public void broadcast(String message) {
        JSONObject JSONtoSend = translateToJSON(message);
        byte[] BytesToPass = JSONtoBytes(JSONtoSend);
        try {
            Main.sender.send(BytesToPass);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(String deviceName, byte[] data) {
        String message = "null";
        try {
            JSONObject messageJSON = translateToJSON(data);
            message = getMessageFromJSON(messageJSON);
        } catch(ParseException e) {

        }
//        ChatMessage cm = new ChatMessage(message.concat(deviceName), Calendar.getInstance());
//        Main.mainActivity.show(cm);
        Log.d("DataRouter", message.concat(deviceName));
    }
}
