package ppt.chat_pz2016.userInterface;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.jar.Manifest;

/**
 * Created by dot on 24.11.16.
 */

public class PermissionsHandler {
    public static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity context, String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
            // TODO: implement this
        } else {
            ActivityCompat.requestPermissions(context, new String[]{permission}, permission.hashCode());
        }
    }
}
