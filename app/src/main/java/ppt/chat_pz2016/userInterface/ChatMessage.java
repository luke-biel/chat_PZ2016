package ppt.chat_pz2016.userInterface;

import java.util.Calendar;

/**
 * Created by Kitsunka on 04.12.2016.
 */

public class ChatMessage {
    public ChatMessage(String message, Calendar date) {
        this.message = message;
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    private String message;

    public Calendar getDate() {
        return date;
    }

    private Calendar date;
}
