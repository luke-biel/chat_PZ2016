package ppt.chat_pz2016.userInterface;

/**
 * Created by dot on 14.10.16.
 */

public interface IUserInterface {
    /*
     * Method used by router to write strings to user
     */
    void print(String message);
}
