package ppt.chat_pz2016;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import ppt.chat_pz2016.userInterface.ChatMessage;
import ppt.chat_pz2016.userInterface.PermissionsHandler;
import ppt.chat_pz2016.dataRouter.DataRouter;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int REQUEST_ENABLE_BT = 1;
    /**
     * request code returned by startActivityForResult
     */
    EditText message;
    /**
     * inputted message
     */

    Button sendButton;
    /**
     * button for sending messages
     */

    DataRouter router;
    /**
     * specific DataRouter
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!PermissionsHandler.hasPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            PermissionsHandler.requestPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        BluetoothManager btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter btAdapter = btManager.getAdapter();
        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }

        message = (EditText) findViewById(R.id.edit_message);
        sendButton = (Button) findViewById(R.id.button);
        sendButton.setOnClickListener(this);

        Main.init(getString(R.string.UUID), this);
    }

    @Override
    public void onClick(View v) {
        sendChatMessage(); Log.d("MainActivity", "Sending message!");
    }


    private void sendChatMessage(){
        String text = message.getText().toString();
        if (text.equals("")) {
            return;
        }
        if (text.length()> 512){
            Toast.makeText(MainActivity.this, "Error: Wiadomość jest zbyt długa",
                    Toast.LENGTH_LONG).show();
            return;
        }

        ChatMessage message = new ChatMessage(text, Calendar.getInstance());

        //router.send(message);
        Main.dataRouter.broadcast(text);
        //router.setContext(message);

        return;
    }

    public void show(ChatMessage message) {
        Calendar date = message.getDate();

        int cHour = date.get(Calendar.HOUR);
        int cMinute = date.get(Calendar.MINUTE);
        int cSecond = date.get(Calendar.SECOND);
        int cDay = date.get(Calendar.DAY_OF_MONTH);
        int cMonth = date.get(Calendar.MONTH);
        int cYear = date.get(Calendar.YEAR);

        String currDate = cYear + "-" + cMonth + "-" + cDay+","+cHour +":"+ cMinute +":"+ cSecond;

        ListView listOfMessages = (ListView)findViewById(R.id.messagesList);
        TextView messageText = (TextView)listOfMessages.findViewById(R.id.message_text);
        TextView messageTime = (TextView)listOfMessages.findViewById(R.id.message_time);


        //messageText.setText(router.getContext());
        //Log.d("isnull", String.valueOf(messageTime));
        //messageTime.setText(currDate);

    }
}


