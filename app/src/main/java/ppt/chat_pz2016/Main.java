package ppt.chat_pz2016;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.UUID;

import ppt.chat_pz2016.communication.IReceiver;
import ppt.chat_pz2016.communication.ISender;
import ppt.chat_pz2016.communication.Receiver;
import ppt.chat_pz2016.communication.Sender;
import ppt.chat_pz2016.dataRouter.DataRouter;
import ppt.chat_pz2016.dataRouter.IDataRouter;
import ppt.chat_pz2016.userInterface.IUserInterface;

/**
 * Created by dot on 05.11.16.
 */

public class Main {
    /**
     * Static insatnce of data router
     */
    public static IDataRouter dataRouter;
    /**
     * Static insatnce of receiver
     */
    public static IReceiver receiver;
    /**
     * Static instance of sender
     */
    public static ISender sender;
    /**
     * Static instance of UI class
     */
    public static IUserInterface ui;
    /**
     * Static instance of Bluetooth Adapter
      */
    public static BluetoothAdapter adapter;
    /**
     * UUID of our application
     */
    public static ParcelUuid UUID;

    public static MainActivity mainActivity;

    /**
     * Initialize static fields of Main
     */
    public static void init(String ID, MainActivity ma) {
        UUID = ParcelUuid.fromString(ID);
        ui = null;
        adapter = BluetoothAdapter.getDefaultAdapter();
        dataRouter = new DataRouter();
        receiver = new Receiver();
        try {
            sender = new Sender();
        } catch (Exception e) {
            Log.e("Main", "Failed to start sender!");
        }
        mainActivity = ma;
        receiver.listen();
        Log.d("Main", "Started application");
    }
}
