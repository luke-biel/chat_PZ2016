package ppt.chat_pz2016;

import org.junit.Before;
import org.junit.Test;

import ppt.chat_pz2016.communication.Daemon;

import static org.junit.Assert.*;

/**
 * Created by dot on 08.11.16.
 */
public class DaemonTest {
    private static final int DefaultWaitTillStop = 247;
    private static final int DefaultWaitTillReset = 65;
    private static final int NewWaitTillStop = 957;
    private static final int NewWaitTillReset = 997;
    private Daemon daemon;

    @Before
    public void before() {
        this.daemon = new Daemon(DefaultWaitTillStop, DefaultWaitTillReset);
    }

    @Test
    public void setWaitTillStop() throws Exception {
        assertEquals(daemon.getWaitTillStop(), DefaultWaitTillStop);
        daemon.setWaitTillStop(NewWaitTillStop);
        assertEquals(daemon.getWaitTillStop(), NewWaitTillStop);
        assertNotEquals(daemon.getWaitTillStop(), DefaultWaitTillStop);
    }

    @Test
    public void setWaitTillRestart() throws Exception {
        assertEquals(daemon.getWaitTillRestart(), DefaultWaitTillReset);
        daemon.setWaitTillRestart(NewWaitTillReset);
        assertEquals(daemon.getWaitTillRestart(), NewWaitTillReset);
        assertNotEquals(daemon.getWaitTillRestart(), DefaultWaitTillReset);
    }

    @Test
    public void isRunning() throws Exception {
        daemon.start();
        assertTrue(daemon.isRunning());
        daemon.shutDown();
        Thread.sleep(daemon.getWaitTillRestart() + daemon.getWaitTillStop());
        assertFalse(daemon.isRunning());
    }
}