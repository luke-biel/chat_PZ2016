package ppt.chat_pz2016;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import org.junit.Ignore;
import org.junit.Test;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.junit.runner.RunWith;

import ppt.chat_pz2016.dataRouter.DataRouter;

import static org.junit.Assert.*;

/**
 * Data Sender tests concept
 *
 *
 */


public class TestDataRoutera {

    DataRouter router = new DataRouter();

    public void JSONTranslationTest(String message ){
        JSONObject testJSON = router.translateToJSON(message);
        assertEquals(router.getMessageFromJSON(testJSON),message);

    }

    public void JSONToBytesTest(String message) throws ParseException{
        JSONObject testJSON = router.translateToJSON(message);
        byte[] testbytes = router.JSONtoBytes(testJSON);
        JSONObject testJSON2 = router.translateToJSON(testbytes);
        assertEquals(testJSON,testJSON2);
    }

    @Test
    public void JSONTTestExamples() throws ParseException{

        JSONTranslationTest("test qwertyuiop");
        JSONTranslationTest("Hello_World & Grubasy :)");
        JSONTranslationTest("Łut szczęścia?");
        JSONToBytesTest("test");
        JSONToBytesTest("Hello_World & Grubasy :)");
        JSONToBytesTest("Łut szczęścia?");
    }

    @Test
    public void JSONtoStringTest() throws ParseException {
        JSONObject testJSON = router.translateToJSON("test qwertyuiop");
        JSONParser parser = new JSONParser();
        String testString = testJSON.toString();
        JSONObject json = (JSONObject) parser.parse(testString);
        assertEquals(testString,json.toString() );
    }

    @Test
    public void StringToBytes(){
        JSONObject testJSON = router.translateToJSON("test qwertyuiop");
        String testString = testJSON.toString();
        byte[] testbytes = testString.getBytes();
        String testString2 = new String(testbytes);
        assertEquals(testString,testString2);
    }



    /*@Test
    public void ISMessageCorrect(String Message){
       assertEquals(CheckMessage(GoodSample1),true);
   assertEquals(CheckMessage(GoodSample2),true);
       assertEquals(CheckMessage(GoodSample3),true);
   assertEquals(CheckMessage(GoodSample4),true);
       assertEquals(CheckMessage(GoodSample5),true);
   assertEquals(CheckMessage(BadSample1),false);
   assertEquals(CheckMessage(BadSample2),false);
   assertEquals(CheckMessage(BadSample3),false);
   assertEquals(CheckMessage(BadSample4),false);
   assertEquals(CheckMessage(BadSample5),false);
    }*/
     /*@Test
     public void MergeTest(String[] Message,string OrginalMessage){
 	assertEquals(mergeMessages(String[] Message,Message.length()),
	OrginalMessage);
     }(*/
}