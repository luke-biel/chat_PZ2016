package ppt.chat_pz2016;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ppt.chat_pz2016.communication.Sender;
import static junit.framework.Assert.*;

/**
 * Created by kovalson on 2016-11-08.
 */

@RunWith( AndroidJUnit4.class )
public class SenderTest
{
    Sender sender;
    byte[] data;

    @Before
    public void createSender() throws Exception
    {
        sender = new Sender();
    }

    @Test
    public void canAdvertiseReturnsTrue()
    {
        assertTrue( sender.canAdvertise() );
    }

    @Test
    public void senderIsNotNull()
    {
        assertNotNull( sender );
    }

    @Test
    public void senderDataIsByteArray()
    {
        assertTrue( data.getClass() == byte[].class );
    }
}
