package ppt.chat_pz2016;

import android.bluetooth.le.ScanResult;

import org.junit.Before;
import org.junit.Test;

import ppt.chat_pz2016.communication.Daemon;
import ppt.chat_pz2016.communication.IReceiver;
import ppt.chat_pz2016.communication.Receiver;

import static org.junit.Assert.*;

/**
 * Created by dot on 08.11.16.
 */
public class ReceiverTest {
    private IReceiver receiver;

    @Before
    public void before() {
        receiver = new Receiver();
    }

    @Test
    public void listen() throws Exception {
        receiver.listen();
        Daemon daemon = ((Receiver)receiver).getDaemon();
        assertNotNull(daemon);
        assertTrue(daemon.isRunning());
    }

    @Test
    public void onReceive() throws Exception {

    }
}